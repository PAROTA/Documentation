include::Menu[Menu]

=== *Need consultation & correction*



Permissions for endpoints
^^^^^^^^^^^^^^^^^^^^^^^^^

 'o' - own
 't' - team
 'a' - all
 '#' - Boleean TRUE
 '-' - no permission assigned

.Users
[options="header" width=100]
|=======================
|endpoint                |request   |permission     |type       |description
1.3+|_/users/_          s|GET       |VIEW_USER      |Value      |'o', 't', 'a'
                        s|PUT       |MODIFY_USER    |Value      |'o', 't', 'a'
                        s|POST      |CREATE_USER    |Value      |'t', 'a'
1.2+|_/users/<user_id>_ s|GET       |VIEW_USER      |Value      |'o', 't', 'a'
                        s|DELETE    |DELETE_USER    |Value      |'o', 't', 'a'
|_/users/login_         s|POST      |LOGIN          |Boolean    |
|_/users/logout_        s|POST      |LOGOUT         |Boolean    |
|_/users/session_       s|GET       |SESSION        |Boolean    |
|=======================

.Teams - *to discuss*
[options="header" width=100]
|=======================
|endpoint                |request   |permission     |type   |description
1.3+|_/teams/_          s|GET       |VIEW_TEAM     |Value  |'o', 'a'
                        s|PUT       |MODIFY_TEAM    |Value  |'o', 'a'
                        s|POST      |CREATE_TEAMS   |Boolean  |
1.2+|_/teams/<team_id>_ s|GET       |VIEW_TEAM      |Value  |'o', 'a'
                        s|DELETE    |DELETE_TEAM    |Boolean |
|=======================

.Shifts
[options="header" width=`00]
|=======================
|endpoint                    |request   |permission     |type    |description
1.2+|_/shifts/_                 s|GET       |VIEW_SHIFTS    |Value   |'o', 't', 'a'
                                s|PUT       |MODIFY_SHIFT   |Value   |'o', 't', 'a'
|_/shifts/<user_id>_            s|DELETE    |DELETE_SHIFT   |Value   |'o', 't', 'a'
|_/shifts/<user_id>/<query>_    s|GET   |VIEW_SHIFT     |Value  |'o', 't', 'a'
|_/shifts/<user_id>/export/<query>_ s|GET   |EXPORT_SHIFT  |Value  |'t', 'a'
|_/shifts/export/<query>_           s|GET   |EXPORT_SHIFT  |Value  |'t', 'a'
|=======================

.Search Engine
[options="header" width=100]
|=======================
|endpoint                |request   |permission     |type   |description
|_/search/<query>_      s|GET       |VIEW_USER      |Value      |'o', 't', 'a'
|=======================


.Notifications - _TODO_
[options="header" width=100]
|=======================
|endpoint                |request   |permission     |type   |description
1.2+|_/notifications_       s|GET      |               |       |
                           s|POST      |               |       |
|_/notifications/changeRequest_       s|POST      |               |       |
|_/notifications/leaveRequest_       s|POST      |               |       |
|=======================